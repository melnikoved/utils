<?php

namespace Mobillogix\ClioBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\Query\Builder;
use Mobillogix\ClioBundle\Document\ClioRecord;
use Mobillogix\ClioBundle\Form\Model\ClioEventFilter;

class ClioRecordRepository extends DocumentRepository
{
    /**
     * @param  ClioRecord $event
     * @return mixed
     */
    public function saveRecord($event)
    {
        $dm = $this->getDocumentManager();

        $dm->persist($event);
        $dm->flush($event);

        return $event;
    }

    /**
     * @param  ClioEventFilter $filter
     * @return Builder
     */
    public function getRecordsQB($filter)
    {
        $qb = $this->createQueryBuilder()->sort("created_at", -1);
        if (is_object($filter)) {
            $this->processFilter($filter, $qb);
        }
        return $qb;
    }

    /**
     * @param ClioEventFilter $filter
     * @param Builder $qb
     */
    private function processFilter($filter, $qb)
    {
        if ($filter->hasId()) {
            $qb->field("id")->equals($filter->getId());
        }
        if ($filter->hasTag()) {
            $qb->field("tags")->in($filter->getTags());
        }
        if ($filter->hasAction()) {
            $qb->field("action")->in($filter->getActions());
        }
        if ($filter->hasType()) {
            $qb->field("type")->in($filter->getTypes());
        }
    }
}
