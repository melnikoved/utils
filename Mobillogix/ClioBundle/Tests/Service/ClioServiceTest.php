<?php


namespace Mobillogix\ClioBundle\Tests\Service;

use Mobillogix\ClioBundle\Tests\StubClioRecord;
use Mobillogix\ClioBundle\Document\ClioRecord;
use Mobillogix\ClioBundle\Model\ClioEvent;
use Mobillogix\ClioBundle\Repository\ClioRecordRepository;
use Mobillogix\ClioBundle\Service\ClioService;
use Mobillogix\ClioBundle\Form\Model\ClioEventFilter;
use Pagerfanta\Pagerfanta;

require_once dirname(__FILE__ ) . '/../StubMongoQueryBuilder.php';

class ClioServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldSaveEvent()
    {
        // GIVEN
        $repository = $this->getClioRecordRepositoryMock();

        $expected = new ClioRecord();
        $expected->setType('type')
            ->setAction('action')
            ->setTags(['tags'])
            ->setParams(['param' => 'value'])
            ->setMeta([
                'meta' => 'value',
                'meta1' => 'value1',
                'meta2' => 'value2',
            ]);

        $repository->expects($this->once())->method('saveRecord')
            ->with($expected);

        $service = new ClioService($repository);
        $service->setContextMeta(['meta1' => 'value1', 'meta' => 'must be override']);

        // WHEN
        $service->addEvent('type', 'action', 'tags', [
            'param' => 'value',
        ], [
            'meta2' => 'value2',
            'meta' => 'value',
        ]);
    }

    public function testShouldReceiveEvents()
    {
        // GIVEN
        $repository = $this->getClioRecordRepositoryMock();

        $record = new StubClioRecord('id');
        $record->setType('type')
            ->setAction('action')
            ->setTags(['tag'])
            ->setParams(['param' => 'value'])
            ->setMeta(['meta' => 'value'])
            ->setCreatedAt(new \DateTime());

        $filter = new ClioEventFilter();
        $filter->setTag("tag");
        $qbMock = $this->getMockBuilder('Doctrine\ODM\MongoDB\Query\Builder')->disableOriginalConstructor()->getMock();
        $repository->expects($this->once())->method('getRecordsQB')
            ->with($filter)
            ->willReturn($qbMock);

        $service = new ClioService($repository);

        // WHEN
        $actual = $service->getRecordsByTagsPager($filter);
        $this->assertTrue($actual instanceof Pagerfanta);
    }

    public function testShouldDecorateEvents()
    {
        // GIVEN
        $record = new StubClioRecord('id');
        $record->setType('type')
            ->setAction('action')
            ->setTags(['tag'])
            ->setParams(['param' => 'value'])
            ->setMeta(['meta' => 'value'])
            ->setCreatedAt(new \DateTime());

        $repository = $this->getClioRecordRepositoryMock();
        $service = new ClioService($repository);

        // WHEN
        $actual = $service->decorateEvents([$record]);

        $expected = new ClioEvent('type', 'action', ['tag'], ['param' => 'value'], ['meta' => 'value'], 'id', new \DateTime(''));
        $this->assertEquals([$expected], $actual);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject|ClioRecordRepository
     */
    protected function getClioRecordRepositoryMock()
    {
        $repository = $this->getMockBuilder('Mobillogix\ClioBundle\Repository\ClioEventRepository')
            ->setMethods(['saveRecord', 'getRecordsQB'])
            ->disableOriginalConstructor()->getMock();

        return $repository;
    }
}

