<?php


namespace Mobillogix\ClioBundle\Tests;

use Mobillogix\ClioBundle\Service\ClioServiceInterface;

class StubClioService implements ClioServiceInterface
{
    /**
     * @inheritdoc
     */
    public function setContextMeta($contextMeta)
    {
    }

    /**
     * @inheritdoc
     */
    public function addEvent($type, $action, $tags, $params = null, $meta = null)
    {
    }

    /**
     * @inheritdoc
     */
    public function getRecordsByTagsPager($filter = null)
    {
    }

    /**
     * @inheritdoc
     */
    public function decorateEvents($records)
    {

    }
}
