<?php


namespace Mobillogix\ClioBundle\Tests\Listener;

use Mobillogix\ClioBundle\Listener\ClioKernelListener;
use Mobillogix\ClioBundle\Tests\StubClioService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Tests\TestHttpKernel;

class ClioKernelListenerTest extends \PHPUnit_Framework_TestCase
{
    public function testShouldStoreRequestData()
    {
        // GIVEN
        /** @var \PHPUnit_Framework_MockObject_MockObject|Request $request */
        $request = $this->getMockBuilder('Symfony\Component\HttpFoundation\Request')
            ->setMethods(['getClientIp', 'getRequestUri', 'getMethod'])->getMock();

        $request->request->add(['some_param' => 'some value']);

        $request->expects($this->once())->method('getClientIp')->willReturn('1.2.3.4');
        $request->expects($this->once())->method('getRequestUri')->willReturn('/some-url');
        $request->expects($this->any())->method('getMethod')->willReturn('POST');

        $request->headers->add([
            'X-Mobillogix-Issuer-Instance' => 'Instance name',
            'X-Mobillogix-Issuer-User' => 'User name',
        ]);

        /** @var \PHPUnit_Framework_MockObject_MockObject|StubClioService $service */
        $service = $this->getMockBuilder('Mobillogix\ClioBundle\Tests\StubClioService')
            ->setMethods(['setContextMeta'])->getMock();

        $meta = [
            'env'   => 'test',
            'issuer_type' => ClioKernelListener::ISSUER_TYPE_API,
            'issuer_ip' => '1.2.3.4',
            'issuer_url' => '/some-url',
            'issuer_method' => 'POST',
            'issuer_request' => ['some_param' => 'some value'],
            'issuer_instance' => 'Instance name',
            'issuer_user' => 'User name',
        ];
        $service->expects($this->once())->method('setContextMeta')
            ->with($meta);

        $listener = new ClioKernelListener($service, "test");

        // WHEN
        $kernel = new TestHttpKernel();
        $listener->onKernelRequest(new GetResponseEvent($kernel, $request, HttpKernelInterface::MASTER_REQUEST));
    }

    public function testShouldStoreCommandData()
    {
        // GIVEN
        /** @var \PHPUnit_Framework_MockObject_MockObject|StubClioService $service */
        $service = $this->getMockBuilder('Mobillogix\ClioBundle\Tests\StubClioService')
            ->setMethods(['setContextMeta'])->getMock();

        $meta = [
            'env'   => 'test',
            'issuer_type' => ClioKernelListener::ISSUER_TYPE_COMMAND,
            'issuer_command' => 'test:command',
            'issuer_cli' => $_SERVER['argv'],
            'issuer_arguments' => ['test' => 'value'],
            'issuer_user' => $this->getCurrentUser(),
        ];
        $service->expects($this->once())->method('setContextMeta')
            ->with($meta);

        $listener = new ClioKernelListener($service, "test");

        // WHEN
        $command = new Command('test:command');
        $input = new StubInput();
        $output = new NullOutput();

        $listener->onCommand(new ConsoleCommandEvent($command, $input, $output));
    }

    private function getCurrentUser()
    {
        $processUser = posix_getpwuid(posix_geteuid());

        return $processUser['name'];
    }
}

class StubInput extends ArrayInput
{
    public function __construct()
    {
    }

    public function getArguments()
    {
        return ['test' => 'value'];
    }

    public function getOptions()
    {
        return ['option' => 'value 1'];
    }

    public function bind(InputDefinition $definition)
    {
    }
}
