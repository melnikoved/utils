<?php


namespace Mobillogix\ClioBundle\Tests;

use Mobillogix\ClioBundle\Document\ClioRecord;

class StubClioRecord extends ClioRecord
{
    public function __construct($id)
    {
        $this->id = $id;
    }
}
