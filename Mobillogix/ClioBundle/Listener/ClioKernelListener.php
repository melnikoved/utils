<?php


namespace Mobillogix\ClioBundle\Listener;

use Mobillogix\ClioBundle\Service\ClioServiceInterface;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class ClioKernelListener
{
    const ISSUER_TYPE_API = 'api';
    const ISSUER_TYPE_COMMAND = 'command';

    /** @var ClioServiceInterface */
    protected $service;

    protected $metaSet = false;

    /**
     * @param ClioServiceInterface $service
     * @param $env
     */
    public function __construct($service, $env)
    {
        $this->service = $service;
        $this->env = $env;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $method = $request->getMethod();

        $meta = [
            'env'           => $this->env,
            'issuer_type'   => self::ISSUER_TYPE_API,
            'issuer_ip'     => $request->getClientIp(),
            'issuer_url'    => $request->getRequestUri(),
            'issuer_method' => $method,
        ];
        if ('GET' != $method) {
            $meta['issuer_request'] = $request->request->all();
        }

        if ($request->headers->has('X-Mobillogix-Issuer-Instance')) {
            $meta['issuer_instance'] = $request->headers->get('X-Mobillogix-Issuer-Instance');
        }
        if ($request->headers->has('X-Mobillogix-Issuer-User')) {
            $meta['issuer_user'] = $request->headers->get('X-Mobillogix-Issuer-User');
        }

        $this->service->setContextMeta($meta);
    }

    public function onCommand(ConsoleCommandEvent $event)
    {
        // Dirty hack. Symfony disappointed.
        $this->bindInput($event);

        $command = $event->getCommand();
        $input = $event->getInput();

        $meta = [
            'env'           => $this->env,
            'issuer_type'   => self::ISSUER_TYPE_COMMAND,
            'issuer_command'   => $command->getName(),
            'issuer_cli'   => $_SERVER['argv'],
            'issuer_arguments'   => $input->getArguments(),
            'issuer_user'   => $this->getCurrentUser(),
        ];

        $this->service->setContextMeta($meta);

        $this->metaSet = true;
    }

    private function getCurrentUser()
    {
        $user = null;
        if (function_exists('posix_getpwuid')) {
            // *NIX
            $processUser = posix_getpwuid(posix_geteuid());
            $user = $processUser['name'];
        } else {
            // Win OS
            $user = getenv('USERNAME');
        }

        return $user;
    }

    private function bindInput(ConsoleCommandEvent $event)
    {
        // merge the application's input definition
        $event->getCommand()->mergeApplicationDefinition();

        $input = $event->getInput();

        // we use the input definition of the command
        $input->bind($event->getCommand()->getDefinition());
    }
}
