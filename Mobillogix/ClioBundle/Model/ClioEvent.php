<?php


namespace Mobillogix\ClioBundle\Model;

class ClioEvent
{
    protected $type;
    protected $action;
    protected $tags;
    protected $params;
    protected $meta;

    protected $persistedId;
    protected $createdAt;

    /**
     * @param string    $type
     * @param string    $action
     * @param array     $tags
     * @param array     $params
     * @param array     $meta
     * @param null      $persistedId
     * @param \DateTime $createdAt
     */
    public function __construct($type, $action, $tags, $params = null, $meta = null, $persistedId = null, $createdAt = null)
    {
        $this->type = $type;
        $this->action = $action;
        $this->tags = $tags;
        $this->params = $params;
        $this->meta = $meta;
        $this->persistedId = $persistedId;
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param  string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param  string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param  array $tags
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param  array|null $params
     * @return self
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param  array|null $meta
     * @return self
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersistedId()
    {
        return $this->persistedId;
    }

    /**
     * @param  mixed $persistedId
     * @return self
     */
    public function setPersistedId($persistedId)
    {
        $this->persistedId = $persistedId;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param  \DateTime|null $createdAt
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
