<?php


namespace Mobillogix\ClioBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(collection="clio_record", repositoryClass="Mobillogix\ClioBundle\Repository\ClioRecordRepository")
 * @ODM\HasLifecycleCallbacks
 */
class ClioRecord
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $type;

    /**
     * @ODM\Field(type="string")
     */
    protected $action;

    /**
     * @ODM\Field(type="collection")
     */
    protected $tags;

    /**
     * @ODM\Field(type="hash")
     */
    protected $params;

    /**
     * @ODM\Field(type="hash")
     */
    protected $meta;

    /**
     * @ODM\Field(type="timestamp", name="created_at"))
     */
    protected $createdAt;

    public function __construct()
    {
    }

    /**
     * @ODM\PrePersist
     */
    public function prePersist()
    {
        $this->createdAt = time();
    }

    /**
     * Get id
     *
     * @return \MongoId $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param  string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set action
     *
     * @param  string $action
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string $action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set tags
     *
     * @param  array $tags
     * @return self
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return array $tags
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set params
     *
     * @param  array $params
     * @return self
     */
    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params
     *
     * @return array $params
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set meta
     *
     * @param  array $meta
     * @return self
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return array $meta
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return self
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
