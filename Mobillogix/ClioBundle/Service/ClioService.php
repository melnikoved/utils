<?php


namespace Mobillogix\ClioBundle\Service;

use Mobillogix\ClioBundle\Document\ClioRecord;
use Mobillogix\ClioBundle\Model\ClioEvent;
use Mobillogix\ClioBundle\Repository\ClioRecordRepository;
// TODO: wrong place. must be in controller!
use Mobillogix\ClioBundle\Service\Util\DoctrineODMMongoDBAdapter;
use Pagerfanta\Pagerfanta;

class ClioService implements ClioServiceInterface
{
    /** @var ClioRecordRepository */
    protected $repository;

    /** @var array */
    protected $contextMeta;

    /**
     * @param ClioRecordRepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return array
     */
    public function getContextMeta()
    {
        return $this->contextMeta;
    }

    /**
     * @param  array $contextMeta
     * @return self
     */
    public function setContextMeta($contextMeta)
    {
        $this->contextMeta = $contextMeta;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addEvent($type, $action, $tags, $params = null, $meta = null)
    {
        $tags = array_unique((array) $tags);

        if (empty($type) || empty($action) || empty($tags)) {
            throw new \Exception("type, action and tags can not be empty.");
        }

        $record = new ClioRecord();
        $record->setType($type)
            ->setAction($action)
            ->setTags((array) $tags)
            ->setParams((array) $params)
            ->setMeta((array) $meta + (array) $this->contextMeta);

        $this->repository->saveRecord($record);
    }

    /**
     * @inheritdoc
     */
    public function getRecordsByTagsPager($filter = null)
    {
        $qb = $this->repository->getRecordsQB($filter);
        $adapter = new DoctrineODMMongoDBAdapter($qb);
        return new Pagerfanta($adapter);
    }

    /**
     * @inheritdoc
     */
    public function decorateEvents($records)
    {
        $result = [];
        foreach ($records as $record) {
            $result[] = $this->decorateEvent($record);
        }

        return $result;
    }

    /**
     * @param ClioRecord $record
     * @return ClioEvent
     */
    private function decorateEvent($record)
    {
        return new ClioEvent($record->getType(),
            $record->getAction(),
            $record->getTags(),
            $record->getParams(),
            $record->getMeta(),
            $record->getId(),
            $record->getCreatedAt()
        );
    }
}

