<?php


namespace Mobillogix\ClioBundle\Service;

use Mobillogix\ClioBundle\Document\ClioRecord;
use Mobillogix\ClioBundle\Model\ClioEvent;
use Mobillogix\ClioBundle\Form\Model\ClioEventFilter;
use Pagerfanta\Pagerfanta;

interface ClioServiceInterface
{
    /**
     * Store context meta from kernel listeners
     * @param $contextMeta
     * @return mixed
     */
    public function setContextMeta($contextMeta);

    /**
     * Add record to history
     *
     * @param string     $type
     * @param string     $action
     * @param array      $tags
     * @param array|null $params
     * @param array|null $meta
     * @return
     */
    public function addEvent($type, $action, $tags, $params = null, $meta = null);

    /**
     * Get records by tags. Optional filters are possible.
     * @param ClioEventFilter $filter
     * @return Pagerfanta
     */
    public function getRecordsByTagsPager($filter = null);

    /**
     * @param ClioRecord[] $records
     * @return ClioEvent[]
     */
    public function decorateEvents($records);
}