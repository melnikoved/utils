<?php


namespace Mobillogix\ClioBundle\Service;


use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;

class ClioServiceNull implements ClioServiceInterface
{
    /**
     * @param  array $contextMeta
     * @return self
     */
    public function setContextMeta($contextMeta)
    {
        $this->contextMeta = $contextMeta;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addEvent($type, $action, $tags, $params = null, $meta = null)
    {
    }

    /**
     * @inheritdoc
     */
    public function getRecordsByTagsPager($filter = null)
    {
        return new Pagerfanta(new ArrayAdapter([]));
    }

    /**
     * @inheritdoc
     */
    public function decorateEvents($records)
    {
        return [];
    }
}
