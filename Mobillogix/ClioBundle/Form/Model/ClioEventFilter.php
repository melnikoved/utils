<?php

namespace Mobillogix\ClioBundle\Form\Model;

class ClioEventFilter
{
    /** @var string */
    private $id;

    /** @var string */
    private $tag;

    /** @var string */
    private $type;

    /** @var string */
    private $action;

    /**
     * @return bool
     */
    public function hasTag()
    {
        return !empty($this->tag);
    }

    /**
     * @return bool
     */
    public function hasType()
    {
        return !empty($this->type);
    }

    /**
     * @return bool
     */
    public function hasAction()
    {
        return !empty($this->action);
    }

    public function hasId()
    {
        return !empty($this->id);
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->explode($this->tag);
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return $this->explode($this->action);
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->explode($this->type);
    }

    private function explode($string)
    {
        if (empty($string)) {
            return [];
        }
        return array_map('trim', explode(',', $string));

    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
}