<?php

namespace Mobillogix\ClioBundle\Controller;

use Mobillogix\ClioBundle\Form\Model\ClioEventFilter;
use Mobillogix\ClioBundle\Form\Type\ClioEventFilterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\View\TwitterBootstrap3View;

/**
 * @Route("/clio")
 */
class ClioController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/create", name="mobillogix_clio_create")
     * @Method(methods={"POST","GET","PUT"})
     */
    public function createTestAction()
    {
        $service = $this->get('mobillogix_clio.clio_service');

        $service->addEvent("web", "test", ["test"]);

        return new Response("Created");
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/{tag}")
     * @Route("/", name="mobillogix_clio_index")
     */
    public function indexAction(Request $request)
    {
        $filter = new ClioEventFilter();
        $form = $this->createForm(ClioEventFilterType::class, $filter);
        $form->handleRequest($request);
        $service = $this->get('mobillogix_clio.clio_service');

        $pager = $service->getRecordsByTagsPager($filter);
        $pager->setCurrentPage(max(1, (integer)$request->query->get('page', 1)));
        $pager->setMaxPerPage(max(5, min(50, (integer)$request->query->get('per_page', 20))));
        $pagerView = new TwitterBootstrap3View();
        $router = $this->container->get('router');
        $routeGenerator = function ($page) use ($router, $pager) {
            return $router->generate('mobillogix_clio_index',
                array('page' => $page, 'per_page' => $pager->getMaxPerPage()));
        };

        $events = $service->decorateEvents($pager->getCurrentPageResults());

        $layout = $this->container->getParameter('mobillogix_clio.layout');
        $template = $this->container->getParameter('mobillogix_clio.template');

        return $this->render($template, [
            'layout' => $layout,
            'filterForm' => $form->createView(),
            'filter' => $filter,
            'events' => $events,
            'eventsPager' => $pager,
            'eventsPagerView' => $pagerView,
            'eventsPagerGenerator' => $routeGenerator,
        ]);
    }
}
