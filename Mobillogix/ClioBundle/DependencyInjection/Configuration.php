<?php

namespace Mobillogix\ClioBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('mobillogix_clio');

        $rootNode->children()
            // Now we only use mongodb service implementation. If new
            ->scalarNode('document_manager')->defaultValue('doctrine_mongodb.odm.default_document_manager')->end()
            ->scalarNode('layout')->end()
            ->scalarNode('template')->defaultValue('MobillogixClioBundle:Clio:index.html.twig')->end()
            ->scalarNode('stop_logging')->defaultValue(false)->end()
            ->scalarNode('force_logging')->defaultValue(false)->end()
        ->end();

        return $treeBuilder;
    }
}
