Utils to be used in Mobilogix projects. 

#Includes:

##ClioBundle:

###Versions:
1. v1  - Symfony2 support
2. v2  - Symfony3 support

###Instalation

Simply run assuming you have installed composer.phar or composer binary:

``` bash
$ php composer.phar require mobillogix-launchpad/utils v2.0
```

### B) Enable the bundle

Enable the bundle in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new \Mobillogix\ClioBundle\MobillogixClioBundle(),
    );
}
```

Finally, add this into config.yml

``` yml
    doctrine_mongodb:
        connections:
            default:
                server: "%mongodb_server%"
                options: {}
        default_database: "%mongodb_database%"
        document_managers:
            default:
                auto_mapping: true

    mobillogix_clio:
        document_manager: doctrine_mongodb.odm.default_document_manager
        layout: ::base.html.twig
        template: :admin:clio.html.twig
        force_logging: true
```